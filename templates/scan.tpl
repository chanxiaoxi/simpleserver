<!-- include header -->
{include file='header.tpl'}

<!-- body -->
	<div class="container theme-showcase dbconn" role="main">
		{foreach $articleList as $article}
		<div class="panel panel-default" id="panel-first">
   			<div class="panel-heading">
      			<h3 class="panel-title">标题：{$article.title}&nbsp;&nbsp;&nbsp;&nbsp;发布时间：{$article.createtime|date_format:"%Y/%m/%d"}</h3>
   			</div>
   			<div class="panel-body">{$article.content} </div>
		</div>
		{/foreach}
    </div> 
<!-- include footer -->
{include file='footer.tpl'}