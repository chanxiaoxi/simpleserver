<!-- include header -->
{include file='header.tpl'}

<!-- body -->
	<div class="container theme-showcase dbconn" role="main">
		<div class="alert alert-info tip">
		<p>关于文章添加的几点说明：<p>
		<p>1.没有添加文字在线编辑器，只有简单的输入框供使用</p>
		<p>2.所有输入数据均未严格校验，测试时请按需求输入</p>
		</div>
      	<form class="form-horizontal dbconn-form" role="form" action="index.php?action=addArticle" method="post">
		  <div class="form-group">
		    <label for="title" class="col-sm-2 control-label">文章标题</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control" id="title" name="title" placeholder="不要留空">
		    </div>
		  </div>
		  <fieldset disabled>
			  <div class="form-group">
			    <label for="createtime" class="col-sm-2 control-label">发布时间</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="createtime" name="createtime" value="{$createtime}">
			    </div>
			  </div>
		  </fieldset>
		  <div class="form-group">
    			<label for="textarea" class="col-sm-2 control-label">内容：</label>
    			<div class="col-sm-10">
			      <textarea class="form-control" rows="3" name="content" id="content"></textarea>
			    </div>  
		  </div>
		  <div class="form-group">
		    <div class="col-sm-offset-2 col-sm-10">
		      <button type="submit" class="btn btn-primary btn-lg">添加文章</button>
		    </div>
		  </div>
		</form>
    </div>
    
    
<!-- include footer -->
{include file='footer.tpl'}