<!-- include header -->
{include file='header.tpl'}

<!-- body -->
	<div class="container theme-showcase" role="main">
    	<!-- Main jumbotron for a primary marketing message or call to action -->
      	<div class="jumbotron">
			<h1>Hello, 跃跃!</h1>
		    <p>接口数据我会以JSON的格式返回给你，调用接口的URL如下：</p>
		    <p>调用所有文章：http://your-address/deomproj/api/findall.php</p>
		    <p>添加文章：http://your-address/demoproj/api/add.php</p>
		    <p>
		    	<a href="index.php" class="btn btn-primary btn-lg" role="button">返回首页</a>
		    </p>
      	</div>
    </div>
<!-- include footer -->
{include file='footer.tpl'}