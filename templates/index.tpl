<!-- include header -->
{include file='header.tpl'}

<!-- body -->
	<div class="container theme-showcase" role="main">
    	<!-- Main jumbotron for a primary marketing message or call to action -->
      	<div class="jumbotron">
			<h1>Hello, 跃跃!</h1>
		    <p>这是系统的入口页面，我以按钮的形式将系统的功能分别列了出来，你以后想扩展的话也容易。</p>
		    <p>
		    	<a href="index.php?action=scan" class="btn btn-primary btn-lg" role="button">查看文章</a>
		    	<a href="index.php?action=addview" class="btn btn-primary btn-lg" role="button">添加文章</a>
		    	<a href="index.php?action=manage" class="btn btn-primary btn-lg" role="button">管理文章</a>
		    	<a href="index.php?action=api" class="btn btn-primary btn-lg" role="button">关于接口</a>
		    </p>
      	</div>
    </div>
<!-- include footer -->
{include file='footer.tpl'}