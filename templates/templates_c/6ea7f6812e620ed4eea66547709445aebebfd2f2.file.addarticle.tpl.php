<?php /* Smarty version Smarty-3.1.17, created on 2014-10-29 12:18:57
         compiled from "templates\addarticle.tpl" */ ?>
<?php /*%%SmartyHeaderCode:94365450bfff7ce759-27328540%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '6ea7f6812e620ed4eea66547709445aebebfd2f2' => 
    array (
      0 => 'templates\\addarticle.tpl',
      1 => 1414578801,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '94365450bfff7ce759-27328540',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.17',
  'unifunc' => 'content_5450bfff8301e8_20277556',
  'variables' => 
  array (
    'createtime' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5450bfff8301e8_20277556')) {function content_5450bfff8301e8_20277556($_smarty_tpl) {?><!-- include header -->
<?php echo $_smarty_tpl->getSubTemplate ('header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>


<!-- body -->
	<div class="container theme-showcase dbconn" role="main">
		<div class="alert alert-info tip">
		<p>关于文章添加的几点说明：<p>
		<p>1.没有添加文字在线编辑器，只有简单的输入框供使用</p>
		<p>2.所有输入数据均未严格校验，测试时请按需求输入</p>
		</div>
      	<form class="form-horizontal dbconn-form" role="form" action="index.php?action=addArticle" method="post">
		  <div class="form-group">
		    <label for="title" class="col-sm-2 control-label">文章标题</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control" id="title" name="title" placeholder="不要留空">
		    </div>
		  </div>
		  <fieldset disabled>
			  <div class="form-group">
			    <label for="createtime" class="col-sm-2 control-label">发布时间</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="createtime" name="createtime" value="<?php echo $_smarty_tpl->tpl_vars['createtime']->value;?>
">
			    </div>
			  </div>
		  </fieldset>
		  <div class="form-group">
    			<label for="textarea" class="col-sm-2 control-label">内容：</label>
    			<div class="col-sm-10">
			      <textarea class="form-control" rows="3" name="content" id="content"></textarea>
			    </div>  
		  </div>
		  <div class="form-group">
		    <div class="col-sm-offset-2 col-sm-10">
		      <button type="submit" class="btn btn-primary btn-lg">添加文章</button>
		    </div>
		  </div>
		</form>
    </div>
    
    
<!-- include footer -->
<?php echo $_smarty_tpl->getSubTemplate ('footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array(), 0);?>
<?php }} ?>
