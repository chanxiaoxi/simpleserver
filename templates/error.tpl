<!-- include header -->
{include file='header.tpl'}

<!-- body -->
	<div class="container theme-showcase dbconn" role="main">
		<div class="alert alert-danger tip">
			<p>系统错误提示：</p>
			<p>{$message}</p>
			<p>&nbsp;</p>
			<p><a href="{$redirectUrl}" class="btn btn-danger" role="button">返回首页</a></p>
		</div>
    </div> 
<!-- include footer -->
{include file='footer.tpl'}