<!-- include header -->
{include file='header.tpl'}

<!-- body -->
	<div class="container theme-showcase" role="main">
    	<!-- Main jumbotron for a primary marketing message or call to action -->
      	<div class="jumbotron">
			<h1>Hello, 跃跃!</h1>
		    <p>这是一个部署向导，当出现这个页面时是因为你第一次使用该项目，或者系统没有找到数据库的配置文件。它会在本地自动创建数据库，这样方便项目的迁移，免得导入数据麻烦！</p>
		    <p><a href="install.php?action=dbconn-guide" class="btn btn-primary btn-lg" role="button">开始配置数据库连接</a></p>
      	</div>
    </div>
<!-- include footer -->
{include file='footer.tpl'}