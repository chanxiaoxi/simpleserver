<!-- include header -->
{include file='header.tpl'}

<!-- body -->
	<div class="container theme-showcase dbconn" role="main">
		<div class="alert alert-info tip">
		<p>关于数据库创建的几点说明：<p>
		<p>1.系统默认为你创建一个新闻文章数据模型，所以这里没有提供表名称，系统默认表名称为article</p>
		<p>2.这个简单的服务器模型只涉及到文章数据模型的曾、删、改、查。</p>
		<p>3.省略了表单校验。</p>
		</div>
      	<form class="form-horizontal dbconn-form" role="form" action="install.php?action=dbconn" method="post">
		  <div class="form-group">
		    <label for="dbhost" class="col-sm-2 control-label">数据库地址</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control" id="dbHost" name="dbHost" placeholder="请输入数据库连接地址，如果服务器在本地，默认为localhost">
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="username" class="col-sm-2 control-label">用户名</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control" id="dbUserName" name="dbUserName" placeholder="请输入数据库用户名，默认为root">
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="password" class="col-sm-2 control-label">密码</label>
		    <div class="col-sm-10">
		      <input type="password" class="form-control" id="dbPwd" name="dbPwd" placeholder="请输入数据库密码，默认为root或空或YES">
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="dbname" class="col-sm-2 control-label">数据库名称</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control" id="dbName" name="dbName" placeholder="请输入要创建的数据库名称，如果不输入，系统默认创建名为test的数据库">
		    </div>
		  </div>
		  <fieldset disabled>
			  <div class="form-group">
			    <label for="dbcharset" class="col-sm-2 control-label">数据库编码</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" id="dbCharset" name="dbCharset" placeholder="数据库默认编码为UTF8，默认不允许修改">
			    </div>
			  </div>
		  </fieldset>
		  <div class="form-group">
		    <div class="col-sm-offset-2 col-sm-10">
		      <button type="submit" class="btn btn-primary btn-lg">创建数据库</button>
		    </div>
		  </div>
		</form>
    </div>
    
    
<!-- include footer -->
{include file='footer.tpl'}