<?php
require_once 'lib/smarty/Smarty.class.php';
require_once 'lib/Db.php';

/**
 * @description Article Model
 * @author Gavin
 *
 */
class Article
{
    private $smarty = '';
    private $db = '';
    
    public function __construct()
    {
        $this->smarty = new Smarty();
        $this->smarty->setTemplateDir('templates/');
        $this->smarty->setCompileDir('templates/templates_c');
        $this->smarty->setConfigDir('inc/');
        $this->smarty->setCacheDir('cache/');
        
        $this->db = new Db();
        
    }
    
    public function init()
    {
        $this->smarty->display('index.tpl');
    }
    
    public function addView()
    {
        $createtime = date('Y-m-d', time());
        $this->smarty->assign('createtime', $createtime);
        $this->smarty->display('addarticle.tpl');
    }
    
    public function addArticle()
    {
        $options = array('title' => $_POST['title'], 'content'=>$_POST['content'], 'createtime'=>time(), 'updatetime'=>time());
        
        if(! $this->db->dbConnByConfig()){
            exit('ERROR!:数据库连接失败！');
        }
        
        $result = $this->db->insert('Article', $options);
        
        if($result){
            $this->smarty->assign('message', '文章添加成功！');
            $this->smarty->display('success.tpl');
            $this->smarty->assign('redirectUrl','index.php');
            $this->smarty->display('redirect.tpl');
        }
        else{
            $this->smarty->assign('message', '文章添加失败！@@');
            $this->smarty->assign('redirectUrl','index.php');
            $this->smarty->display('error.tpl');
        }
        
        $this->db->dbClose();
    }
    
    public function scan()
    {
        if(! $this->db->dbConnByConfig()){
            exit('ERROR!:数据库连接失败！');
        }
        
        $arrArticle = $this->db->findAll('article');
        
        if(count($arrArticle)){
            $this->smarty->assign('articleList', $arrArticle);
            $this->smarty->display('scan.tpl');
            $this->smarty->assign('redirectUrl','index.php');
            $this->smarty->display('redirect.tpl');
        }
        else{
            $this->smarty->assign('message', '文章查询失败！@@');
            $this->smarty->assign('redirectUrl','index.php');
            $this->smarty->display('error.tpl');
        }
        
        $this->db->dbClose();
    }
    
    public function api()
    {
        $this->smarty->display('api.tpl');
    }
}

?>