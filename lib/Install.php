<?php
/**
 * @description install class
 * @author Gavin
 *
 */
require_once 'lib/smarty/Smarty.class.php';
require_once 'lib/Db.php';


class Install
{
    private $smarty = '';
    
    public function __construct()
    {
        $this->smarty = new Smarty();
        $this->smarty->setTemplateDir('templates/');
        $this->smarty->setCompileDir('templates/templates_c');
        $this->smarty->setConfigDir('inc/');
        $this->smarty->setCacheDir('cache/');
    }
    
    public function init()
    {
        $this->smarty->display('install.tpl');
    }
    
    public function dbConnGuide()
    {
        $this->smarty->display('dbconn.tpl');
    }
    
    public function dbConn()
    {   
        //get post data
        $dbHost = empty($_POST['dbHost']) ? 'localhost' : trim($_POST['dbHost']);
        $dbUserName = empty($_POST['dbUserName']) ? 'root' : trim($_POST['dbUserName']);
        $dbPwd = empty($_POST['dbPwd']) ? '' : trim($_POST['dbPwd']);
        $dbName = empty($_POST['dbName']) ? 'test' : trim($_POST['dbName']);
        
        //create database 
        $db = new Db($dbHost, $dbUserName, $dbPwd);
        if($db->dbConn() & $db->createDb($dbName)){
            $this->smarty->assign('message', '数据库创建成功！');
            $this->smarty->display('success.tpl');
            $db->dbClose();
        }
        else{
            $this->smarty->assign('message', '数据库创建过程中发生未知错误@@');
            $this->smarty->assign('redirectUrl','index.php');
            $this->smarty->display('error.tpl');
            exit();
        }
        
        //create table
        $db = new Db($dbHost, $dbUserName, $dbPwd);
        $dbconn = $db->dbConnByName($dbName);
        if($dbconn & $db->createTable()){
            $this->smarty->assign('message', '数据表创建成功！');
            $this->smarty->display('success.tpl');
            $db->dbClose();
        }
        else{
            $this->smarty->assign('message', '数据表创建过程中发生未知错误@@');
            $this->smarty->assign('redirectUrl','index.php');
            $this->smarty->display('error.tpl');
            exit();
        }
        
        //create config file
        $configStr = "[database config]\r\n".
               "dbHost = $dbHost \r\n".
               "dbUserName = $dbUserName \r\n".
               "dbPwd = $dbPwd \r\n".
               "dbName = $dbName \r\n".
               "dbCharset = utf8";
        $handle = fopen('inc/dbconfig.ini', 'w');
        $result = fwrite($handle, $configStr);
        fclose($handle);
        if($result){
            $this->smarty->assign('message', '配置文件创建成功！');
            $this->smarty->display('success.tpl');
            $this->smarty->assign('redirectUrl','index.php');
            $this->smarty->display('redirect.tpl');
        }
        else{
            $this->smarty->assign('message', '配置文件创建过程中发生未知错误@@');
            $this->smarty->assign('redirectUrl','index.php');
            $this->smarty->display('error.tpl');
        }
        
    }
}
