<?php
/**
 * @description database function lib
 * @author Gavin
 *
 */
class Db
{
    private $_dbHost = '';
    private $_dbUserName = '';
    private $_dbPassword = '';
    private $_dbName = '';
    private $_dbCharset = 'utf8';
    private $_dbh='';
    
    public function __construct($dbHost = '', $dbUserName = '', $dbPassword = '')
    {
        $this->_dbHost = $dbHost;
        $this->_dbUserName = $dbUserName;
        $this->_dbPassword = $dbPassword;
    }
    
    public function dbConn()
    {
        try{
            $this->_dbh = new PDO('mysql:host='.$this->_dbHost, $this->_dbUserName, $this->_dbPassword);
        }
        catch(PDOException $e){
            exit('ERROR!:'.$e->getMessage());
        }

        return ($this->_dbh === null) ? FALSE : TRUE;
    }
    
    public function dbConnByName($dbName)
    {
        try{
            $this->_dbh = new PDO('mysql:host='.$this->_dbHost.';dbname='.$dbName, $this->_dbUserName, $this->_dbPassword);
        }
        catch(PDOException $e){
            exit('ERROR!:'.$e->getMessage());
        }
        
        return ($this->_dbh === null) ? FALSE : TRUE;
    }
    
    public function dbconnByConfig($fileDir='')
    {
        $file = empty($fileDir) ? 'inc/dbconfig.ini' : $fileDir;
        if(! file_exists($file)){
            exit('ERROR!:找不到数据库配置文件！');
        }
        $options = parse_ini_file($file);
        
        if(! count($options)){
            exit('ERROR!:配置文件解析错误！');
        }
        
        try{
            $this->_dbh = 
                new PDO('mysql:host='.$options['dbHost'].';dbname='.
                    $options['dbName'], $options['dbUserName'], $options['dbPwd'], 
                    array(PDO::MYSQL_ATTR_INIT_COMMAND=>"SET NAMES 'utf8'"));
        }
        catch(PDOException $e){
            exit('ERROR!:'.$e->getMessage());
        }
        
        return ($this->_dbh === null) ? FALSE : TRUE;
    }
    
    public function createDb($dbName)
    {
        $dbName = empty($dbName) ? 'test' : $dbName;
        $querySQL = "DROP DATABASE IF EXISTS $dbName ;CREATE DATABASE $dbName DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci";
        $stmt = $this->_dbh->prepare($querySQL);
        $result = $stmt->execute();
        
        return $result;
    }
    
    public function createTable($tableName = 'article')
    {
        $querySQL = "CREATE TABLE $tableName (".
            "id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY,".
            "title VARCHAR(225) NOT NULL,".
            "content TEXT NOT NULL,".
            "createtime INT(11) NOT NULL,".
            "updatetime INT(11) NOT NULL)";
        $stmt = $this->_dbh->prepare($querySQL);
        $result = $stmt->execute();
        
        return $result;
            
    }
    
    public function insert($tableName, $options)
    {
        $querySQL = "INSERT INTO $tableName(title, content, createtime, updatetime) VALUES(?,?,?,?)";
        $stmt = $this->_dbh->prepare($querySQL);
        if(count($options) > 0){
            $stmt->bindParam(1, $options['title']);
            $stmt->bindParam(2, $options['content']);
            $stmt->bindParam(3, $options['createtime']);
            $stmt->bindParam(4, $options['updatetime']);
        }
        $result = $stmt->execute();
        
        return $result;
    }
    
    public function findAll($tableName)
    {
        $querySQL = "SELECT * FROM $tableName ORDER BY id DESC";
        $stmt = $this->_dbh->prepare($querySQL);
        $result = $stmt->execute();
        if(! $result){
            exit('ERROR!:查询失败！');
        }
        $array = $stmt->fetchAll();
        
        return $array;
    }
    
    public function dbClose()
    {
        $this->_dbh = null;
    }
}

?>