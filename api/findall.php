<?php
require_once '../lib/Db.php';

header('Content-Type:text/json;charset=utf-8');

$db = new Db();
$dbconn = $db->dbconnByConfig('../inc/dbconfig.ini');
if(! $dbconn){
    $data = array('ret'=>0, 'msg'=>'db_connect_failed');
    echo json_encode($data, JSON_UNESCAPED_UNICODE);
}
$articleList = $db->findAll('article');
$parserList = array();
// print_r($articleList);
// die();
// $encode_arr = array('UTF-8','ASCII','GBK','GB2312','BIG5','JIS','eucjp-win','sjis-win','EUC-JP');
// foreach ($articleList as $article){
//     $parserList[]['title'] = mb_convert_encoding($article['title'], 'UTF-8', $encode_arr);
//     $parserList[]['content'] = mb_convert_encoding($article['content'], 'UTF-8', $encode_arr);
// //     $parserList[]['title'] = urlencode($article['title']);
// //     $parserList[]['content'] = urlencode($article['content']);
//     $parserList[]['id'] = $article['id'];
//     $parserList[]['createtime'] = $article['createtime'];
//     $parserList[]['updatetime'] = $article['updatetime'];
// }
echo json_encode($articleList, JSON_UNESCAPED_UNICODE);

