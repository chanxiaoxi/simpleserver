<?php
require_once 'lib/smarty/Smarty.class.php';
require_once 'lib/Article.php';

if(! file_exists("inc/dbconfig.ini")){
    //redirect to install.php
    header("Location:install.php");
    exit();
}

$action = empty($_GET['action']) ? 'index' : $_GET['action'];
$article = new Article();

switch ($action){
    case 'index' :
        $article->init();
        break;
    case 'addview':
        $article->addView();
        break;
    case 'addArticle':
        $article->addArticle();
        break;
    case 'scan' :
        $article->scan();
        break;
    case 'api':
        $article->api();
        break;
    default:
        exit('ERROR!:no such action find!');
}


