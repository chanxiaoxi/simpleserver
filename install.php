<?php
require_once 'lib/Install.php';

$action = empty($_GET['action']) ? 'init' : $_GET['action'];
$install = new Install();

switch($action){
    case 'init':
        $install->init();
        break;
    case 'dbconn-guide':
        $install->dbConnGuide();
        break;
    case 'dbconn':
        $install->dbConn();
        break;
    default:
        exit('no such action is find in Object Install');
}